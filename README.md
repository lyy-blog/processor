### JSR 269: Pluggable Annotation Processing API
* 插入式注解处理API(JSR 269)提供一套标准API在编译器来处理Annotations
* 建立语言本身的一个模型来表达JAVA语言元素,将JAVA语言映射成对象
* 本例子是在编译的是把@Advice注解过的方法前后加入打印的代码
* 用maven打包成 processor.jar 然后编译对应的测试代码
* javac -cp processor.jar XXXX.jar
