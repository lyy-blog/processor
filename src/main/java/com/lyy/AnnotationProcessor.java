package com.lyy;

import com.sun.source.util.Trees;
import com.sun.tools.javac.processing.JavacProcessingEnvironment;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Name;
import com.sun.tools.javac.util.Names;

import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;

import static com.sun.tools.javac.tree.JCTree.JCExpression;
import static com.sun.tools.javac.tree.JCTree.JCMethodDecl;

/**
 * 注解处理在被@Advice  注解方法的前后植入代码
 */
@SupportedAnnotationTypes("com.lyy.*")
public class AnnotationProcessor extends AbstractProcessor {

    private Trees trees;
    private TreeMaker treeMaker;
    private Name.Table names;

    private void note(String msg) {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, msg);
    }

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        trees = Trees.instance(processingEnv);
        Context context = ((JavacProcessingEnvironment) processingEnv).getContext();
        treeMaker = TreeMaker.instance(context);
        names = Names.instance(context).table;
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        for (Element element : roundEnv.getElementsAnnotatedWith(Advice.class)) {
            if (element.getKind() == ElementKind.METHOD) {
                JCTree.JCMethodDecl methodDecl = (JCMethodDecl) trees.getTree(element);
                updateTestMethod(methodDecl);
            }
        }
        return false;
    }

    private void updateTestMethod(JCMethodDecl methodDecl) {
        JCExpression printExpression = getPrintExpress("before------------>");
        JCExpression printExpression1 = getPrintExpress("after------------>");
        List<JCTree.JCStatement> list = List.of(treeMaker.Exec(printExpression), methodDecl.body, treeMaker.Exec(printExpression1));
        methodDecl.body = treeMaker.Block(0, list);
    }

    private JCExpression getPrintExpress(String message){
        JCExpression printExpression = treeMaker.Ident(getName("System"));
        printExpression = treeMaker.Select(printExpression, getName("out"));
        printExpression = treeMaker.Select(printExpression, getName("println"));
        List<JCExpression> printArgs = List.from(new JCExpression[]{treeMaker.Literal(message)});
        return treeMaker.Apply(List.<JCExpression>nil(), printExpression, printArgs);
    }

    private Name getName(String string) {
        return names.fromString(string);
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.values()[SourceVersion.values().length - 1];
    }
}